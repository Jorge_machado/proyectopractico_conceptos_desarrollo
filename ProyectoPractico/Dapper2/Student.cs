﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper2
{
    public class Student
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int StandardId { get; set; }
    }
}
