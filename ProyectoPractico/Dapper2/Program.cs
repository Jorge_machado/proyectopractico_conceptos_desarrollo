﻿using Dapper;
using System;
using System.Data.SqlClient;

namespace Dapper2
{
    class Program
    {
        static void Main(string[] args)
        {
            string connection =
                @"Data Source=DESKTOP-LVJNNNI\SQLEXPRESS;Initial Catalog=SchoolDB;Integrated Security=True";
            Console.WriteLine("Conexión DB exitosa!!");


            //db.(QUERY y EXECUTE) son extensiones que DAPPER le da la clase SqlConnection
            using (var db = new SqlConnection(connection))
            {
                //insertar Student con DAPPER
                var sqlInsert = "insert into Student(FirstName, LastName) Values(@FirstName,@LastName)";
                var resultInsert = db.Execute(sqlInsert, new{ FirstName= "Pablo", LastName = "Perez" });
                Console.WriteLine("Se creo el estudiante!!");

                //editar x StudentId con DAPPER
                var sqlEdit = "update Student set FirstName=@FirstName,LastName=@LastName where StudentId=@StudentId";
                var resultEdit = db.Execute(sqlEdit, new { FirstName = "Felipe", LastName = "Gonzales", StudentId = 2});
                Console.WriteLine("Se edito el estudiante!");

                //listar Student con DAPPER
                Console.WriteLine(" - Lista de estudiantes - ");
                var sqlList = "select StudentId, FirstName, LastName from Student";   
                var resultList = db.Query<Student>(sqlList);
                foreach (var oElement in resultList)
                {
                    Console.WriteLine(oElement.StudentId + " " + oElement.FirstName + " " + oElement.LastName);
                }
            }
        }
    }
}
