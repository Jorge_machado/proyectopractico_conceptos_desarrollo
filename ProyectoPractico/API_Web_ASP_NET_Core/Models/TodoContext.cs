﻿using Microsoft.EntityFrameworkCore;

namespace API_Web_ASP_NET_Core.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoItemDTO> TodoItemDTOs { get; set; }
    }
}
