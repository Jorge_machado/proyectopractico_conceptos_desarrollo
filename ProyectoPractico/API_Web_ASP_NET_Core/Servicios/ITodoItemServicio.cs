﻿using API_Web_ASP_NET_Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API_Web_ASP_NET_Core.Servicios
{
    public interface ITodoItemServicio
    {
        Task<IEnumerable<TodoItemDTO>> Listar_TodoItem(TodoItemDTO todoItemDTO);
        //Task<TodoItem> Listar_TodoItem(TodoItem todoItem);
        Task<TodoItem> Guardar_TodoItem(TodoItem todoItem);
        Task<TodoItem> Seleccionar_TodoItem_x_id(int id);
        Task<TodoItem> Actualizar_TodoItem_x_id(int id);
        Task<TodoItem> Borrar_TodoItem_x_id(int id);
    }
}
