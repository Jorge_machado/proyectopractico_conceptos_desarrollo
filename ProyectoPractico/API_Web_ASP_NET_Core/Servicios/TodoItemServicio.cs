﻿using API_Web_ASP_NET_Core.Models;
using API_Web_ASP_NET_Core.Repositorios;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API_Web_ASP_NET_Core.Servicios
{
    public class TodoItemServicio : ITodoItemServicio
    {
        private readonly ITodoItemRepositorio _ITodoItemRepositorio;

        public TodoItemServicio(ITodoItemRepositorio iTodoItemRepositorio)
        {
            _ITodoItemRepositorio = iTodoItemRepositorio;   
        }


        public async Task<TodoItem> Actualizar_TodoItem_x_id(int id)
        {
            return await _ITodoItemRepositorio.Actualizar_TodoItem_x_id(id);    
        }

        public async Task<TodoItem> Borrar_TodoItem_x_id(int id)
        {
            return await _ITodoItemRepositorio.Borrar_TodoItem_x_id(id); 
        }

        public async Task<TodoItem> Guardar_TodoItem(TodoItem todoItem)
        {
            return await _ITodoItemRepositorio.Guardar_TodoItem(todoItem);    
        }

        //public async Task<IEnumerable<TodoItemDTO>> Listar_TodoItem(TodoItemDTO todoItemDTO)
        //{
        //    return await _ITodoItemRepositorio.Listar_TodoItem();
        //}

        public async Task<IEnumerable<TodoItemDTO>> Listar_TodoItem(TodoItemDTO todoItemDTO)
        {
            return await _ITodoItemRepositorio.Listar_TodoItem(todoItemDTO);
        }

        //public async Task<TodoItem> Listar_TodoItem(TodoItem todoItem)
        //{
        //    return await _ITodoItemRepositorio.Listar_TodoItem(todoItem); 
        //}

        public async Task<TodoItem> Seleccionar_TodoItem_x_id(int id)
        {
            return await _ITodoItemRepositorio.Seleccionar_TodoItem_x_id(id);
        }
    }
}
