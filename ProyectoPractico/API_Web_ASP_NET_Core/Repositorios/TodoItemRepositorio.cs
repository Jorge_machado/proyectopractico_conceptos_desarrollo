﻿using API_Web_ASP_NET_Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Web_ASP_NET_Core.Repositorios
{
    public class TodoItemRepositorio : ITodoItemRepositorio
    {
        private readonly TodoContext _todoContext;
        public TodoItemRepositorio(TodoContext todoContext)
        {
            _todoContext = todoContext;
        }

            public Task<TodoItem> Actualizar_TodoItem_x_id(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<TodoItem> Borrar_TodoItem_x_id(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<TodoItem> Guardar_TodoItem(TodoItem todoItem)
        {
            TodoItem Resultado = default;

            try
            {
                var todoItem2 = new TodoItem
                {
                    IsComplete = todoItem.IsComplete,
                    Name = todoItem.Name
                };

                _todoContext.TodoItems.Add(todoItem2);
                await _todoContext.SaveChangesAsync();

                Resultado = todoItem2;

                return Resultado;
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        //public Task<TodoItem> Listar_TodoItem(TodoItem todoItem)
        //{
        //    throw new System.NotImplementedException();
        //}

        public Task<TodoItem> Seleccionar_TodoItem_x_id(int id)
        {
            throw new System.NotImplementedException();
        }



        private bool TodoItemExists(long id) =>
             _todoContext.TodoItems.Any(e => e.Id == id);

        private static TodoItemDTO ItemToDTO(TodoItem todoItem) =>
                                                new TodoItemDTO
                                                {
                                                    Id = todoItem.Id,
                                                    Name = todoItem.Name,
                                                    IsComplete = todoItem.IsComplete
                                                };

        public async Task<IEnumerable<TodoItemDTO>> Listar_TodoItem(TodoItemDTO todoItemDTO)
        {
            return await _todoContext.TodoItems
                .Select(x => ItemToDTO(x))
                .ToListAsync();
        }
    }
}
