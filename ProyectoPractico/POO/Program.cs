﻿using System;
using System.Xml.Linq;

namespace POO
{
    internal class Program
    {
        #region MAIN
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var personaEncapsulamiento = new PersonaEncapsulamiento() { Nombre = "Ana"};
            Console.WriteLine("Utilizando Encapsulamiento: " + "Nombre:" + personaEncapsulamiento.Nombre + 
                              " EdadEncapsulamiento:" + personaEncapsulamiento.ObtenerEdad().ToString());

            var personaHerencia = new PersonaHerencia() { Nombre = "Pedro" };
            var dominicano = new Dominicano() { Nombre = "Juan", Cedula = "001" };
            dominicano.ObtenerEdad();
            Console.WriteLine("Utilizando Herencia: " + "Nombre1:" + personaHerencia.Nombre + 
                              " Edad1:" + personaHerencia.ObtenerEdad() + " Nombre2:" + dominicano.Nombre + 
                              " Cédula:" + dominicano.Cedula + " Edad2:" + dominicano.ObtenerEdad() + 
                              " Le Gusta El Mango:" + dominicano.LeGustaElMango);

        }
        #endregion

        #region ENCAPSULAMIENTO
        //no se permite acceder a FechaNacimiento (private)
        //solo utiliza el metodo para devolver la edad 
        public class PersonaEncapsulamiento
        { 
            public string Nombre { get; set; }
            private DateTime FechaNacimiento;

            public int ObtenerEdad()
            {
                int edad = 0;
                edad = DateTime.Now.Year - FechaNacimiento.Year;
                return edad;
            }
        }
        #endregion

        #region HERENCIA
        public class PersonaHerencia
        {
            public string Nombre { get; set; }
            private DateTime FechaNacimiento;

            public int ObtenerEdad()
            {
                int edad = 0;
                edad = DateTime.Now.Year - FechaNacimiento.Year;
                return edad;
            }
        }
        public class Dominicano : PersonaHerencia
        {
            public string Cedula { get; set; }
            public bool LeGustaElMango => true;
        }
        public void Ejemplo()
        {
            var dominicano = new Dominicano() { Nombre = "Juan", Cedula = "001" };
            dominicano.ObtenerEdad();   
        }
        #endregion

        #region POLIMORFISMO
        public interface IEnviaDatos
        {
            bool EsFinDelArchivo();
            byte EnviarDato();
        }

        public interface IRecibeDatos
        {
            void RecibirDato(byte v);
        }

        public class Copiador
        {
            public void Copiar(IEnviaDatos emisor, IRecibeDatos receptor)
            {
                while (emisor.EsFinDelArchivo())
                { 
                    receptor.RecibirDato(emisor.EnviarDato()); 
                }
            }
        }

        public class MiSoftware
        { 
            public void Ejemplo()
            {
                var copiador = new Copiador();
                //copiador.Copiar(new Teclado(), new Pantalla());
                //copiador.Copiar(new Teclado(), new Impresora());
                //copiador.Copiar(new TabletaWacom(), new Pantalla());
                //copiador.Copiar(new TabletaWacom(), new Pantalla());
            }
        }


        #endregion

        #region INVERSIÓN DE DEPENDENCIAS

        public interface IAplicacion
        {
            string MostrarMensajeEspera();
            string MostarMensajeExitoso();
            string MostarMensajeError();
        }
        public interface IRepositorio
        {
            string InsertarElemento(string elemento);
        }

        public class Negocios
        {
            private readonly IAplicacion _IAplicacion;
            private readonly IRepositorio _IRepositorio;

            public Negocios(IAplicacion IAplicacion, IRepositorio IRepositorio)
            {
                this._IAplicacion = IAplicacion;
                this._IRepositorio = IRepositorio;
            }

            public void InsertarElemento(string elemento)
            {
                _IAplicacion.MostrarMensajeEspera();
                var resultado = _IRepositorio.InsertarElemento(elemento);
                if (resultado != null)
                {
                    _IAplicacion.MostarMensajeExitoso();
                }
                else
                {
                    _IAplicacion.MostarMensajeError();
                }
            }
        }

        #endregion

    }
}
